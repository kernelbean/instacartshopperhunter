__author__ = 'kernelbean'

from wtforms import Form, BooleanField, StringField, SelectField, validators

class ConfirmForm(Form):
    zipcode = StringField('Zip code', [validators.Length(min=5, max=25)])
    mobile_type = SelectField('Mobile Model',
                              choices=[('', 'Select your smart phone model'),
                                       ('iphone', 'iPhone'),
                                       ('android', 'Android'),
                                       ('wp', 'Windows Phone'),
                                       ('bb', 'BlackBerry'),
                                       ('other', 'Others')])