__author__ = 'kernelbean'

from flask import Blueprint, render_template, request, jsonify
from flask_login import login_required, login_user, logout_user, current_user
from ..models.shopper import create_shopper, login_shopper
from ..forms.apply_form import ApplyForm
from ..forms.login_form import LoginForm
from ..forms.confirm_form import ConfirmForm
from ..forms.update_form import UpdateForm
from ..errors import AlreadyExisted
from ..models.shopper import Shopper

from logbook import Logger

log = Logger("views-api")

bp = Blueprint('api', __name__ , url_prefix ='/api', template_folder = 'templates')


@bp.route('/apply', methods=['POST'])
def apply():
    # initialize the apply form
    apply_form = ApplyForm(request.form)
    if request.method == 'POST' and apply_form.validate():
        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')
        email = request.form.get('email')
        mobile = request.form.get('mobile')
        source = request.form.get('source')

        try:
            shopper = create_shopper(first_name, last_name, mobile, email, source)
            if shopper is not None and shopper.id > 0:
                # the user is successfully inserted
                login_user(shopper, remember=True)
                return jsonify({'succeed': True, 'user_guid': shopper.id })
        except AlreadyExisted:
            log.error('User registration failed. Email already existed')
            return jsonify({'succeed': False, 'reason': {'email':['This user already exist']}})

    return jsonify({'succeed': False, 'reason': apply_form.errors})


@bp.route('/submit', methods=['POST'])
@login_required
def submit():
    user = current_user
    confirm_form = ConfirmForm(request.form)
    if confirm_form.validate():
        mobile_type = request.form.get('mobile_type')
        zipcode = request.form.get('zipcode')

        # submit the application
        shopper = Shopper(user)
        shopper.submit_application(mobile_type, zipcode)

        return jsonify({'succeed':True})

    return jsonify({'succeed': False, 'reason': confirm_form.errors})


@bp.route('/login', methods=['POST'])
def login():

    login_form = LoginForm(request.form)

    if request.method == 'POST':
        if login_form.validate():
            last_name = request.form.get('last_name')
            email = request.form.get('email')
            mobile = request.form.get('mobile')
            user = login_shopper(last_name, mobile, email)

            if(user is not None): # user is found

                login_user(user, remember=True)

                return jsonify({'succeed':True, 'user_guid': user.id})
            else:
                # TODO:
                # a better model here is to count the failure times and blacklist the user
                return jsonify({'succeed': False, 'invalid': True})

    return jsonify({'succeed':False, 'reason':login_form.errors})


@bp.route('/update', methods=['POST'])
@login_required
def update():
    user = current_user
    update_form = UpdateForm(request.form)

    if update_form.validate():
        shopper = Shopper(user)
        shopper.update_info(update_form.mobile.data,
                            update_form.mobile_type.data,
                            update_form.zipcode.data)
        log.info("Successfully updated user's info")
        return jsonify({'succeed':True})

    return jsonify({'succeed':False, 'reason':update_form.errors})