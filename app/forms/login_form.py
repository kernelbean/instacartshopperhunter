__author__ = 'kernelbean'
from wtforms import Form, StringField, validators

class LoginForm(Form):
    last_name = StringField('Last Name', [validators.Length(min=1, max=25), validators.InputRequired()])
    email = StringField('Email Address', [validators.Length(min=3, max=35), validators.InputRequired()])
    mobile = StringField('Mobile', [validators.Length(min=6,max=35), validators.InputRequired()])