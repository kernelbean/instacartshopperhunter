__author__ = 'kernelbean'

import unittest
from app import app, db
from app.models.shopper import create_shopper, Shopper
from app.constants import APPLY_STATUS_QUIZ_STARTED, APPLY_STATUS_QUIZ_COMPLETED
from app.errors import AlreadyExisted


class ModelsTest(unittest.TestCase):
    """
    Test the user data model
    """
    def setUp(self):
        app.config.from_object('configs.test')
        self.app = app.test_client()
        db.create_all()


    def tearDown(self):
        db.session.remove()
        db.drop_all()


    def test_status(self):
        user = create_shopper('first', 'last','123456', 'k@g.com', 'web')
        assert user.apply_status == APPLY_STATUS_QUIZ_STARTED
        shopper = Shopper(user)
        shopper.submit_application('wp','94404')
        assert user.apply_status == APPLY_STATUS_QUIZ_COMPLETED


    def test_creation(self):
        """
        create a duplicate user, expect exception
        :return:
        """
        create_shopper('user', 'user','123456', 'user@g.com', 'web')
        with self.assertRaises(AlreadyExisted):
            create_shopper('user2','user2','654321','user@g.com','')


    def test_invalid_input(self):
        with self.assertRaises(ValueError):
            create_shopper('','','','','')


if __name__ == '__main__':
    unittest.main()


