__author__ = 'kernelbean'

import unittest
from app.statistic import Statistic


class DateParseTest(unittest.TestCase):
    """
    Test the parse functions of the statistic class
    """
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_date_parse(self):
        stat = Statistic()
        assert stat.__parse_week_of_year__('201501') == '2015-01-05-2015-01-11'
        assert stat.__parse_week_of_year__('201452') == '2014-12-29-2015-01-04'

    def test_date_parse_empty(self):
        stat = Statistic()
        with self.assertRaises(ValueError):
            stat.__parse_week_of_year__('')

    def test_date_parse_error(self):
        stat = Statistic()
        with self.assertRaises(ValueError):
            stat.__parse_week_of_year__('abc')

    def test_date_validate(self):
        stat = Statistic()
        assert stat.__validate_date__('2015-01-01') == True
        assert stat.__validate_date__('2015-2-2') == True
        assert stat.__validate_date__('20150101') == False
        assert stat.__validate_date__('2015-1-32') == False
        assert stat.__validate_date__('abc') == False
        assert stat.__validate_date__('') == False
        assert stat.__validate_date__(None) == False

if __name__ == '__main__':
    unittest.main()


