__author__ = 'kernelbean'

import unittest
from app import app, db
from app.models.shopper import create_shopper
from datetime import datetime
from datetime import timedelta
from app.statistic import Statistic


class FunnelsTest(unittest.TestCase):
    """
    Test the funnels.json report
    """
    def setUp(self):
        app.config.from_object('configs.test')
        self.app = app.test_client()
        db.create_all()
        self.__create_users__()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def __create_users__(self):
        start_date = datetime.strptime('11/26/14', '%m/%d/%y')
        # 11/25/14 is a Wednesday, therefore, we can know if the first week has the numbers
        for i in range(45):

            # try to make two week to have zero applicants, 3 is Wednesday
            if i in range(14,28):
                continue

            user = create_shopper('first', 'last', '123456', '%s@g.com' % str(i), '')

            # assign the date/2 so that we can have lower number of buckets
            user.created_time = start_date + timedelta(i)

            # assign the application status
            user.apply_status = i % 6
            db.session.add(user)
        db.session.commit()

    def test_with_out_dates(self):
        stat = Statistic(use_cache=False)
        result = stat.run()
        expected = {'2014-12-29-2015-01-04': {'applied': 7, 'onboarding_completed': 2, 'quiz_completed': 1, 'rejected': 1, 'hired': 1, 'onboarding_requested': 1, 'quiz_started': 1}, '2014-12-22-2014-12-28': {'applied': 5, 'onboarding_completed': 0, 'quiz_completed': 1, 'rejected': 1, 'hired': 1, 'onboarding_requested': 1, 'quiz_started': 1}, '2015-01-05-2015-01-11': {'applied': 5, 'onboarding_completed': 0, 'quiz_completed': 1, 'rejected': 1, 'hired': 1, 'onboarding_requested': 1, 'quiz_started': 1}, '2014-12-08-2014-12-14': {'applied': 2, 'onboarding_completed': 0, 'quiz_completed': 1, 'rejected': 0, 'hired': 0, 'onboarding_requested': 0, 'quiz_started': 1}, '2014-12-01-2014-12-07': {'applied': 7, 'onboarding_completed': 1, 'quiz_completed': 1, 'rejected': 2, 'hired': 1, 'onboarding_requested': 1, 'quiz_started': 1}, '2014-11-24-2014-11-30': {'applied': 5, 'onboarding_completed': 1, 'quiz_completed': 1, 'rejected': 0, 'hired': 1, 'onboarding_requested': 1, 'quiz_started': 1}}
        assert result == expected

    def test_empty_results(self):
        stat = Statistic(use_cache=False)
        result = stat.run('2014-11-01', '2014-11-25')
        assert result == {}

    def test_limited_range(self):
        stat = Statistic(use_cache=False)
        result = stat.run('2014-11-25', '2014-11-28')
        expected = {'2014-11-24-2014-11-30': {'applied': 3, 'onboarding_completed': 0, 'quiz_completed': 1, 'rejected': 0, 'hired': 0, 'onboarding_requested': 1, 'quiz_started': 1}}
        assert result == expected

    def test_invalid_input(self):
        stat = Statistic(use_cache=False)
        with self.assertRaises(ValueError):
            stat.run('','abc')

if __name__ == '__main__':
    unittest.main()


