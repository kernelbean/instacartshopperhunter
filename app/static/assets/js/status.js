// confirm page js
// vincent 2015/11/1


$(document).ready(function () {

    ajaxSetup()

    $("#agreecheck").click(function(){
        var agreed = this.checked;
        if(agreed)
        {
            $('#confirm_form_submit').removeAttr('disabled');
        }
        else
        {
            $('#confirm_form_submit').attr('disabled','disabled');
        }
    })

    $('#update_form').on('submit', function() {
        mobile = $('#update_form input[name="mobile"]').val();
        zipcode = $('#update_form input[name="zipcode"]').val();
        mobile_type = $('#update_form select[name="mobile_type"]').val();

        $("#update_form_submit").prop('disabled',true)
        $("#update_form_submit").text("WORKING...");

        resetErrors("confirm_form");

        $.ajax({
            type: "POST",
            url: "/api/update",
            data: { 'mobile': mobile, 'zipcode': zipcode, 'mobile_type': mobile_type },
            success: function(results) {
                if(results.succeed)
                {
                    swal({
                    title: "Done!",
                    text: "Your profile is updated!",
                    type: "success",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                    }, function(isConfirm)
                    {
                    })
                }
                else
                {
                    form_error_handler('update_form', results.reason);
                }

                $("#update_form_submit").text("update");
                $("#update_form_submit").prop('disabled',false)

            },
            error: function(error) {
                console.log(error);
                $("#update_form_submit").text("update");
                $("#update_form_submit").prop('disabled',false)
            }
        })
    });
});