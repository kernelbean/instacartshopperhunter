__author__ = 'kernelbean'

from app import db
import datetime


class User(db.Model):
    """
    CREATE TABLE Shopper (
        id BIGINT NOT NULL AUTO_INCREMENT,
        first_name VARCHAR(45) NOT NULL,
        last_name VARCHAR(45) NOT NULL,
        email VARCHAR(120) NOT NULL,
        mobile VARCHAR(45) NOT NULL,
        source VARCHAR(120) NULL,
        created_time DATETIME NOT NULL,
        last_logon DATETIME NULL,
        apply_status INT NULL,
        mobile_type VARCHAR(120) NULL,
        zipcode VARCHAR(45) NULL,
        authenticated BOOLEAN,
        PRIMARY KEY(id)
    )
    """

    __tablename__ = 'Shopper'

    id = db.Column(db.Integer, primary_key = True)
    first_name = db.Column(db.String(45))
    last_name = db.Column(db.String(45))
    email = db.Column(db.String(45))
    mobile = db.Column(db.String(45))
    created_time = db.Column(db.DateTime, default = datetime.datetime.utcnow())
    source = db.Column(db.String(120))
    last_logon = db.Column(db.DateTime)
    apply_status = db.Column(db.Integer, default = 0)
    mobile_type = db.Column(db.String(120))
    zipcode = db.Column(db.String(45))
    authenticated = db.Column(db.Boolean, default = False)


    def __init__(self, first_name, last_name, email, mobile, source, created_timestamp = None):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.mobile = mobile
        self.source = source
        if(created_timestamp is not None):
            self.created_time = created_timestamp
        else:
            self.created_time = datetime.datetime.utcnow()

    def is_active(self):
        return True

    def get_id(self):
        return self.id

    def is_authenticated(self):
        return self.authenticated






