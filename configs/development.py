__author__ = 'kernelbean'

DEBUG = True

import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:instacart123@localhost/instacart_shopper'

REDIS_URL= 'redis://localhost:6379/0'

CSRF_ENABLED = True

CSRF_SESSION_KEY = 'instacart-shopper-vincent-k1s2h3l4z5'

# signing cookies
SECRET_KEY = 'instacart-shopper-vincent-a2d5h9lk'