__author__ = 'kernelbean'

from app import app

app.config.from_object('configs.production')

if __name__ == '__main__':
    app.run()
