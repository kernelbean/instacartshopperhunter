__author__ = 'kernelbean'

from flask import Flask
from flask_wtf import CsrfProtect
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_redis import FlaskRedis



app = Flask(__name__, static_folder='static', static_url_path='')

# # sql database
db = SQLAlchemy(app)

# redis db
redis_store = FlaskRedis(app)

from models.user import User
from views.pages import bp as page_view
from views.api import bp as api_view

#CSRF protection
csrf = CsrfProtect()
login_manager = LoginManager()

csrf.init_app(app)

# login manager
login_manager.init_app(app)
login_manager.login_view = '/'


# add the page routes
app.register_blueprint(page_view)
app.register_blueprint(api_view)


@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)




