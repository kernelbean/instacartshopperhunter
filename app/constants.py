__author__ = 'kernelbean'


# applciation started, but not agreed yet to background check
APPLY_STATUS_QUIZ_STARTED = 0

# agreed to background check, wait for approval
APPLY_STATUS_QUIZ_COMPLETED = 1

# onboard requested
APPLY_STATUS_ONBOARD_REQUESTED = 2

# onboard started
APPLY_STATUS_ONBOARD_COMPLETED = 3

# hired
APPLY_STATUS_HIRED = 4

# rejected
APPLY_STATUS_REJECTED = 5


