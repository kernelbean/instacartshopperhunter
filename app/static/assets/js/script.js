// main page js
// vincent 2015/11/1

function initNavbar() {

    var scrollSpeed = 750;
    var scrollOffset = 50;
    var easing = 'swing';

    $('#navbar-top .navbar-default ul.nav').onePageNav({
        currentClass: 'active',
        changeHash: false,
        scrollSpeed: scrollSpeed,
        scrollOffset: scrollOffset,
        scrollThreshold: 0.5,
        filter: ':not(.external)',
        easing: easing
    });

    $('.nav-external').click(function (e) {
        e.preventDefault();
        $('html, body').stop().animate({
            scrollTop: $($(this).attr("href")).offset().top - scrollOffset
        }, scrollSpeed, easing);
    });

    $('#navbar-top .navbar-default').affix({
        offset: {
            top: $('#home').height()
        }
    });
}

function initAnimations() {
    $('.animated').appear(function () {
        var el = $(this);
        var animation = el.data('animation');
        var delay = el.data('delay');
        if (delay) {
            setTimeout(function () {
                el.addClass(animation);
                el.addClass('showing');
                el.removeClass('hiding');
            }, delay);
        } else {
            el.addClass(animation);
            el.addClass('showing');
            el.removeClass('hiding');
        }
    }, {
        accY: -60
    });

    // step hover
	$('.service').hover(function(){
		$('i', this).addClass('animated tada');
	},function(){	
        $('i', this).removeClass('animated tada');
	});
}

function initForms()
{
    $(".applyBtn").click(function()
    {

        $("#applyDialog").modal('show');
        // hack. the tab show doesn't work with animation
        $("#LoginTab").removeClass('active');
        $("#ApplyTab").addClass('active')

    });
    $(".loginBtn").click(function()
    {

        $("#applyDialog").modal('show');
        // hack. the tab show doesn't work with animation
        $("#ApplyTab").removeClass('active');
        $("#LoginTab").addClass('active')

    });

    $('#apply_form').on('submit', function() {

        return true;
    });

    $('#apply_form').on('submit', function() {
        first_name = $('#apply_form input[name="first_name"]').val();
        last_name = $('#apply_form input[name="last_name"]').val();
        email = $('#apply_form input[name="email"]').val();
        mobile = $('#apply_form input[name="mobile"]').val();
        source = $('#apply_form input[name="source"]').val();
        $("#apply_form_submit").prop('disabled',true)
        $("#apply_form_submit").text("WORKING...");

        resetErrors("apply_form");


        $.ajax({
            type: "POST",
            url: "/api/apply",
            data: { 'first_name': first_name,
                    'last_name': last_name,
                    'mobile': mobile,
                    'email': email,
                    'source': source },
            success: function(results) {
                console.log(results);
                if(results.succeed)
                {
                    location.href="/login";
                }
                else
                {
                    form_error_handler("apply_form", results.reason);
                }
                 $("#apply_form_submit").text("Apply");
                 $("#apply_form_submit").prop('disabled',false)
            },
            error: function(error) {
                console.log(error);
                $("#apply_form_submit").text("Apply");
                $("#apply_form_submit").prop('disabled',false)
            }
        })
    });


    $('#login_form').on('submit', function() {
        name = $('#login_form input[name="last_name"]').val();
        email = $('#login_form input[name="email"]').val();
        mobile = $('#login_form input[name="mobile"]').val();

        $("#login_form_submit").prop('disabled',true)
        $("#login_form_submit").text("WORKING...");

        resetErrors("login_form");

        $.ajax({
            type: "POST",
            url: "/api/login",
            data: { 'last_name': name, 'email': email, 'mobile': mobile },
            success: function(results) {
                if(results.succeed)
                {
                    location.href="/login";
                }
                else
                {
                    if(results.invalid)
                    {
                        $("#invalid_account_msg").show();
                    }
                    else
                    {
                        $("#invalid_account_msg").hide();
                        form_error_handler('login_form', results.reason);
                    }
                }
                $("#login_form_submit").text("Login");
                $("#login_form_submit").prop('disabled',false)
            },
            error: function(error) {
                console.log(error);
                $("#login_form_submit").text("Login");
                $("#login_form_submit").prop('disabled',false)
            }
        })
    });

}

$(document).ready(function () {
    initNavbar();
    initAnimations();
    ajaxSetup();
    initForms();

    // Instacart commercial video
    $(".player").mb_YTPlayer();

});

$(window).load(function () {
    $(".loader .fading-line").fadeOut();
    $(".loader").fadeOut("slow");
});
