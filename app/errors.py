#: Common Error Codes


#: user already existed
ALREADY_EXISTED = 0x01

#: the session id is not found
SESSION_NOT_FOUND = 0x02

class ShopperHunterError(Exception):
    """A ShopperHunter customized error

    """

    __slots__ = (
        'code',
        'description',
        'id',
        'tracing',
    )

    def __init__(
        self,
        description=None,
        id=None,
        tracing=None,
    ):
        super(ShopperHunterError, self).__init__(description)
        self.tracing = tracing
        self.id = id
        self.description = description

    @classmethod
    def from_code(cls, code, **kw):
        return {
            ALREADY_EXISTED: AlreadyExisted,
            SESSION_NOT_FOUND: SessionNotFound
        }[code](**kw)

class AlreadyExisted(ShopperHunterError):
    code = ALREADY_EXISTED

class SessionNotFound(ShopperHunterError):
    code = SESSION_NOT_FOUND
