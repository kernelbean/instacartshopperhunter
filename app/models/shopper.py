__author__ = 'kernelbean'

from app import db
from .user import User
from sqlalchemy import exc, exists, func

from ..errors import AlreadyExisted
from logbook import Logger
import logging
from ..constants import APPLY_STATUS_QUIZ_COMPLETED


if __debug__:
    logging.basicConfig()
    logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

log = Logger('ShopperClass')


def create_shopper(first_name, last_name, mobile, email, source):
    """
    create a new shopper
    :param first_name:
    :param last_name:
    :param mobile:
    :param email:
    :return: the new shopper's id, or exception
    """
    if email is None or email == '' \
            or first_name is None or first_name == '' \
            or last_name is None or last_name == ''\
            or mobile is None or mobile == '':
        log.error("Assertion failed for create_shopper")
        raise ValueError('Invalid input')

    (existed, ), = db.session.query(exists().where(func.lower(User.email) == func.lower(email)))

    if(existed):
        raise AlreadyExisted('User already existed')

    new_user = User(first_name, last_name, email, mobile, source)
    try:
        db.session.add(new_user)
        db.session.commit()
        return new_user
    except exc.SQLAlchemyError:
        return None


def login_shopper(last_name, mobile, email):
    """
    Login with a shopper's mobile and email and last name
    :param last_name:
    :param mobile:
    :param email:
    :return:
    """
    if(last_name is None or last_name==''
       or mobile is None or mobile == ''
       or email is None or email == ''):
        log.error('Assertion failed for login_shopper')
        return 0
    try:
        user = db.session.query(User).filter(func.lower(User.last_name) == func.lower(last_name),
                                             User.mobile == mobile,
                                             func.lower(User.email) == func.lower(email)).first()
        if user is None:
            log.error('User is not found')
            return user

        user.authenticated = True
        db.session.add(user)
        db.session.commit()

        return user

    except exc.SQLAlchemyError as e:
        log.error('exception while querying user %s' % e)
        return 0


def logout_shopper(user):
    user.authenticated = False
    db.session.add(user)
    db.session.commit()


class Shopper(object):
    def __init__(self, user):
        self.user = user

    def submit_application(self, mobile_type, zipcode):
        """
        submit the application
        :param mobile_type:
        :param zipcode:
        :return:
        """
        self.__update_info__(mobile_type, zipcode)
        self.user.apply_status = APPLY_STATUS_QUIZ_COMPLETED
        self.__commit__()

    def get_status(self):
        """
        get the application status
        :return:
        """
        return self.user.apply_status

    def update_info(self, mobile, mobile_type, zipcode):
        """
        update the shopper's information
        :param mobile_type:
        :param zipcode:
        :return:
        """
        self.user.mobile = mobile
        self.__update_info__(mobile_type, zipcode)
        self.__commit__()

    def __update_info__(self, mobile_type, zipcode):
        self.user.mobile_type = mobile_type
        self.user.zipcode = zipcode

    def __commit__(self):
        db.session.add(self.user)
        db.session.commit()