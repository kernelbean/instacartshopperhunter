// confirm page js
// vincent 2015/11/1


$(document).ready(function () {

    ajaxSetup()

    $("#agreecheck").click(function(){
        var agreed = this.checked;
        if(agreed)
        {
            $('#confirm_form_submit').removeAttr('disabled');
        }
        else
        {
            $('#confirm_form_submit').attr('disabled','disabled');
        }
    })

    $('#confirm_form').on('submit', function() {
        zipcode = $('#confirm_form input[name="zipcode"]').val();
        mobile_type = $('#confirm_form select[name="mobile_type"]').val();

        $("#confirm_form_submit").prop('disabled',true)
        $("#confirm_form_submit").text("WORKING...");

        resetErrors("confirm_form");

        $.ajax({
            type: "POST",
            url: "/api/submit",
            data: { 'zipcode': zipcode, 'mobile_type': mobile_type },
            success: function(results) {
                if(results.succeed)
                {
                    $("#confirm_form_submit").text("application submitted");
                    $("#confirm_form_submit").prop('disabled',true)
                    swal({
                    title: "Done!",
                    text: "Your shopper application is submitted!",
                    type: "success",
                    confirmButtonText: "Check my status",
                    closeOnConfirm: false
                    }, function(isConfirm)
                    {
                        location.href='/login';
                    })
                }
                else
                {
                    form_error_handler('confirm_form', results.reason);
                     $("#confirm_form_submit").text("submit my application!");
                    $("#confirm_form_submit").prop('disabled',false)

                }

            },
            error: function(error) {
                console.log(error);
                $("#confirm_form_submit").text("submit my application!");
                $("#confirm_form_submit").prop('disabled',false)
            }
        })
    });
});