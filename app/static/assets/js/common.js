// common js lib for shopper site
// vincent 2015/11/1

// setup the csrf token from meta header
function ajaxSetup()
{

    var csrftoken = $('meta[name=csrf-token]').attr('content');

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if(!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
}

// handle the form validations
function form_error_handler(id, reasons)
{
    if(reasons == null)
    {
        return;
    }

    $.each(reasons, function(i,v){
                        var msg = '<label class="control-label" for="' + i + '">' + v.join('<br>') + '</label>';
                        var input =  $('#' + id + ' input[name="' + i + '"]');
                        input.closest('.form-group').addClass('has-error');
                        input.after(msg);
                    });

    var keys = Object.keys(reasons);

    if(keys.length>0)
    {
        $('#' + id + ' input[name="' + keys[0] + '"]').focus();
    }
}

function resetErrors(id) {
    $('#' + id + ' input, #' + id + ' select').closest('.form-group').removeClass('has-error');
    $('#' + id + ' label.control-label').remove();
}
