__author__ = 'kernelbean'

from app import app

app.config.from_object('configs.development')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
