__author__ = 'kernelbean'

from . import db
from . import redis_store
from datetime import datetime
import time
import pickle

PREFIX_CACHE = 'FUNNEL'

class Statistic(object):
    def __init__(self, use_cache = True, time = 60):
        '''
        Get statistis
        :param use_cache: do we use redis for the result
        :param time: expire time in seconds for the redis cache. default is 60 seconds
        :return:
        '''
        self.use_cache = use_cache
        self.time = time

    def run(self, from_date = None, to_date = None):
        '''
        Run a statistic for the funnel report
        :param from_date:
        :param to_date:
        :return:
        '''

        condition = ''

        if not from_date and not to_date:
            condition = ''
        else:
            if self.__validate_date__(from_date) and self.__validate_date__(to_date):
                condition = 'WHERE date(created_time) BETWEEN \'%s\' and \'%s\'' % (from_date, to_date)
            else:
                # one of the date is incorrect
                raise ValueError(u"Invalid date format. 'start_date' and 'end_date' will be formatted like '2014-12-22'.")

        # build the redis key
        key = PREFIX_CACHE + from_date + to_date if condition != '' else PREFIX_CACHE + 'ALL'

        # read from redis if use cache
        if self.use_cache:
            pickled = redis_store.get(key)
            if pickled:
                return pickle.loads(pickled)

        # SQL Statement
        # *************
        # Because mySQL divide a week between Sunday to Saturday
        # But the requirement of the project asks for Monday to Sunday.
        # Therefore, we substracted by 1 day in order to meet the project's requirement

        statement = '''SELECT
                                count(*) as total,
                                count(apply_status=0 or null) as started,
                                count(apply_status=1 or null) as submitted,
                                count(apply_status=2 or null) as onboard_requested,
                                count(apply_status=3 or null) as onboard_completed,
                                count(apply_status=4 or null) as hired,
                                count(apply_status=5 or null) as rejected,
                                YEARWEEK(DATE(DATE_ADD(created_time, INTERVAL -1 DAY))) as yweek
                            FROM
                                Shopper
                            %s
                            GROUP BY
                                yweek
                    ''' % condition

        result = db.engine.execute(statement)
        stat = {}
        for row in result:
            week_of_year = str(row[7])
            date_range = self.__parse_week_of_year__(week_of_year)
            item = {'applied': row[0],
                    'quiz_started': row[1],
                    'quiz_completed': row[2],
                    'onboarding_requested': row[3],
                    'onboarding_completed': row[4],
                    'hired': row[5],
                    'rejected': row[6]}

            stat[date_range]= item

        if self.use_cache:
            redis_store.set(key, pickle.dumps(stat), self.time)

        return stat

    @staticmethod
    def __validate_date__(str):
        if(str is None):
            return False
        try:
            datetime.strptime(str, '%Y-%m-%d')
            return True
        except ValueError:
            return False

    @staticmethod
    def __parse_week_of_year__(week_of_year):

        if week_of_year is None or len(week_of_year)!=6:
            raise ValueError('Invalid input')

        try:
            # get monday from week of year
            monday_time = time.strptime('%s %s 1' % (week_of_year[:4], week_of_year[-2:]), '%Y %W %w')
            # get sunday from week of year
            sunday_time = time.strptime('%s %s 0' % (week_of_year[:4], week_of_year[-2:]), '%Y %W %w')
            # format date to string
            monday = datetime.fromtimestamp(time.mktime(monday_time)).strftime('%Y-%m-%d')
            sunday = datetime.fromtimestamp(time.mktime(sunday_time)).strftime('%Y-%m-%d')
            return '%s-%s' % (monday, sunday)
        except ValueError as e:
            raise ValueError('Failed to convert date')