__author__ = 'kernelbean'

from wtforms import Form, BooleanField, StringField, SelectField, validators
from confirm_form import ConfirmForm

class UpdateForm(ConfirmForm):
    mobile = StringField('Mobile', [validators.Length(min=6,max=35), validators.InputRequired()])