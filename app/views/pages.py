__author__ = 'kernelbean'

from flask import Blueprint, render_template, redirect, url_for, jsonify, request
from flask_login import login_required, login_user, logout_user, current_user
from ..forms.apply_form import ApplyForm
from ..forms.login_form import LoginForm
from ..forms.confirm_form import ConfirmForm
from ..forms.update_form import UpdateForm
from logbook import Logger
from ..constants import APPLY_STATUS_QUIZ_STARTED, APPLY_STATUS_QUIZ_COMPLETED
from ..models.shopper import logout_shopper, Shopper
from ..statistic import Statistic

log = Logger("views-pages")

bp = Blueprint('page', __name__ , url_prefix='', template_folder='templates')


def render_homepage():
    apply_form = ApplyForm()
    login_form = LoginForm()
    return render_template('index.html', apply_form = apply_form, login_form = login_form)


@bp.route('/', methods=['GET'])
def home():
    return render_homepage()


@bp.route('/confirm', methods=['GET'])
@login_required
def confirm():
    """
    Need agree background check on this page
    :return:
    """
    user = current_user
    if user is None:
        return render_homepage()
    else:
        shopper = Shopper(user)
        if shopper.get_status() != APPLY_STATUS_QUIZ_STARTED:
            # user already agreed here
            return redirect(url_for('page.status'))
        confirm_form = ConfirmForm()
        return render_template('confirm.html', user=user, confirm_form = confirm_form)


@bp.route('/status',methods=['GET'])
@login_required
def status():
    """
    Check current status after agree to background check
    :return:
    """

    user = current_user
    if user is None:
        return render_homepage()
    else:
        shopper = Shopper(user)
        if shopper.get_status() == APPLY_STATUS_QUIZ_STARTED:
            return redirect(url_for('page.confirm'))
        else:
            update_form = UpdateForm(mobile=user.mobile, mobile_type=user.mobile_type, zipcode=user.zipcode)
            return render_template("status.html", user=user, update_form=update_form);


@bp.route('/login', methods=['GET'])
@login_required
def login_get():
    """
    Login to user's application page
    :return:
    """
    user = current_user
    if user.apply_status == APPLY_STATUS_QUIZ_STARTED:
        return redirect(url_for('page.confirm'))
    elif user.apply_status == APPLY_STATUS_QUIZ_COMPLETED:
        return redirect(url_for('page.status'))
    else:
        return render_homepage()


@bp.route('/logout', methods=['GET'])
@login_required
def logout():
    user = current_user
    logout_shopper(user)
    logout_user()
    return redirect(url_for('page.home'))


@bp.route('/funnels.json', methods=['GET'])
def analysis():
    stat = Statistic()
    try:
        result = stat.run(request.args.get('start_date'), request.args.get('end_date'))
        return jsonify(result)
    except ValueError as e:
        return jsonify({'result':'failed', 'reason': e.message})
